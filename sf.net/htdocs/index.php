<?php require_once("ibm-acpi_lib.php");?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>ibm-acpi - IBM ThinkPad ACPI Extras Driver</title>
<link rev="made" href="mailto:akw@users.sf.net">
<link rel="shortcut icon" href="favicon.ico">
<meta name="Keywords" content="ibm-acpi,ibm,thinkpad,acpi,linux,extras,driver,unix,open source,free,gpl">
<meta name="Description" content="This is a Linux ACPI driver for the IBM ThinkPad laptops. It aims to support various features of these laptops which are accessible through the ACPI framework but not otherwise supported by the generic Linux ACPI drivers.">
<meta name="Modified" content="2007-01-13 14:50:00">
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#0000FF" vlink="#800080" alink="#FF0000">
<table border="0" width="100%">
<tr><td width="100%">
<center><h1>ibm-acpi - IBM ThinkPad ACPI Extras Driver</h1>
<img src="images/tux.png" alt="Linux" border="0"> 
<img src="images/acpi.png" alt="ACPI" border="0">
</center>
</td><td align="right" valign="top">
<img src="images/thinkpad.jpg" alt="IBM Thinkpad" width="180" height="152" border="0">
</td></tr></table>
<h3>This is a Linux ACPI driver for the IBM ThinkPad laptops. It aims to
support various features of these laptops which are accessible through
the ACPI framework but not otherwise supported by the generic Linux
ACPI drivers.</h3>

<p><b>Features</b></p>
<p>
The features currently supported are the following (see the 
<a href="README">README</a> for detailed description):</p>
<ul>
<li>Fn key combinations (hotkey)</li>
<li>Bluetooth enable and disable (bluetooth)</li>
<li>video output switching, expansion control (video)</li>
<li>ThinkLight on and off (light)</li>
<li>limited docking and undocking (dock)</li>
<li>UltraBay eject (bay)</li>
<li>CMOS control (cmos)</li>
<li>LED control (led)</li>
<li>ACPI sounds (beep)</li>
<li>temperature sensors (thermal)</li>
<li><font color=red>Experimental: embedded controller register dump (ecdump)</font></li>
<li>LCD brightness control (brightness)</li>
<li>volume control (volume)</li>
<li><font color=red>Experimental: fan speed, fan enable/disable (fan)</font></li>
</ul>
<p><b>Compatibility</b></p>
The driver has been tested on the following ThinkPad models:
<p>
<?php show_compatibilities();?>
<p>
Please submit updates to <a href="mailto:ibm-acpi@hmh.eng.br">ibm-acpi@hmh.eng.br</a>

<p><b>Latest releases</b></p>
<ul>
<li>ibm-acpi latest releases are currently available through the <a href="http://sourceforge.net/project/showfiles.php?group_id=117042">sourceforge.net file release download system</a>;</li>
<li>ibm-acpi development releases are available in a public <a href="http://git.or.cz">git</a> tree graciously hosted by the <a href="http://repo.or.cz">repo.or.cz</a> project at <a href="http://repo.or.cz/w/linux-2.6/linux-acpi-2.6/ibm-acpi-2.6.git">http://repo.or.cz/w/linux-2.6/linux-acpi-2.6/ibm-acpi-2.6.git</a>.</li>
</ul>
The ibm-acpi driver is part of the Linux kernel
<a href="http://www.kernel.org/pub/linux/kernel/v2.6/linux-2.6.10.tar.bz2"
>2.6.10</a> and later (option <code>CONFIG_ACPI_IBM</code>).  The version
included in the kernel may be older than the latest release available from
this web site.
<p>
Older releases are also <a href="https://sourceforge.net/project/showfiles.php?group_id=117042">available</a>.
<br />
<br />
<b>Interesting links related to this project</b>
<ul>
<li><a href="https://sourceforge.net/projects/ibm-acpi/">ibm-acpi Sourceforge project summary</a>
<li><a href="https://lists.sourceforge.net/mailman/listinfo/ibm-acpi-devel">ibm-acpi-devel Mailing list</a>
<li><a href="http://www.thinkwiki.org/">ThinkWiki (Linux Thinkpad Wiki)</a>
<li><a href="http://mailman.linux-thinkpad.org/mailman/listinfo/linux-thinkpad">Linux-Thinkpad Mailinglist</a>
<li><a href="http://acpi.sourceforge.net">acpi.sourceforge.net</a>
<li><a href="http://www.kernel.org">www.kernel.org</a>
</ul>
<hr />
<table border=0 width="100%">
<tr><td>
<a href="http://sourceforge.net"><img src="http://sourceforge.net/sflogo.php?group_id=117042&amp;type=1" width="88" height="31" ALT="SourceForge Logo" border="0"></a>
<!-- <a href="http://sourceforge.net/donate/index.php?group_id=117042"><img src="http://images.sourceforge.net/images/project-support.jpg" width="88" height="32" border="0" alt="Support This Project" /></a> -->
<a href="http://validator.w3.org/check?uri=referer"><img border="0" src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01!" height="31" width="88"></a>
</td>
<td align="right" valign="top">
<?php /* Show modification date */
$stamp=max(filemtime('index.php'),filemtime('compatibility.csv'));
echo "Last modified: ".date ("d.n.Y H:i:s.",$stamp+3600*9)." MEST";
?> 
&copy; 
<a href="mailto:hmholschuh@users.sf.net">hmh</a>,
<a href="mailto:borislav@users.sf.net">borislav</a>, 
<a href="mailto:akw@users.sf.net">akw</a>
</td></tr>
</table>
</body>
</html>
