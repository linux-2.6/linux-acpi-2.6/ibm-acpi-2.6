<?php
// PHP Library for ibm-acpi.sf.net
// (c) Arno Willig
// 2004-10-20 12:30:00

function show_compatibilities() {
  $handle = fopen ("compatibility.csv","r");
  if ($handle) {
    echo "<table border=1>\n";
    $row=0; $cols=0;
    while ( ($data = fgetcsv ($handle, 1000, ",")) !== FALSE ) {
      if (substr($data[0],0,1)<>"#") {
        echo "<tr align=center>";
        for ($c=0; $c<count($data); $c++) {
          if (count($data)>$cols) $cols=count($data);
          if ($row==0) {
	    if (substr($data[$c],0,1)=="!")
	      echo "<th><font color=red>".htmlentities(substr($data[$c],1))."</font></th>";
	    else
	      echo "<th>".htmlentities($data[$c])."</th>";
	  }
          else {
            if ($data[$c]=="+") $entry="<img src=\"images/working.png\" alt=\"Working\" border=0 width=\"42\" height=\"15\">";
            else if ($data[$c]=="=") $entry="<img src=\"images/working_partially.png\" alt=\"Partially working\" border=0 width=\"42\" height=\"15\">";
            else if ($data[$c]=="-") $entry="<img src=\"images/not_working.png\" alt=\"Not working\" border=0 width=\"42\" height=\"15\">";
            else if ($data[$c]=="x") $entry="<img src=\"images/not_supported.png\" alt=\"Not supported\" border=0 width=\"16\" height=\"16\">";
            else if ($data[$c]=="?") $entry="<img src=\"images/not_tested.png\" alt=\"Not tested\" border=0 width=\"16\" height=\"16\">";
            else $entry=htmlentities($data[$c]);
            echo "<td>".$entry."</td>";
            
          }
        }
        echo "</tr>\n";
        $row++;
      }
    }
    echo "<tr><td colspan=\"".$cols."\"><b>Legend:</b> ";
    echo "<img src=\"images/working.png\" alt=\"Working\" border=0 width=\"42\" height=\"15\"> Working &nbsp;&nbsp;";
    echo "<img src=\"images/working_partially.png\" alt=\"Partially working\" border=0 width=\"42\" height=\"15\"> Partially working &nbsp;&nbsp;";
    echo "<img src=\"images/not_working.png\" alt=\"Not working\" border=0 width=\"42\" height=\"15\"> Not working &nbsp;&nbsp;";
    echo "<img src=\"images/not_supported.png\" alt=\"Not supported\" border=0 width=\"16\" height=\"16\"> Not supported &nbsp;&nbsp;";
    echo "<img src=\"images/not_tested.png\" alt=\"Not tested\" border=0 width=\"16\" height=\"16\"> Not tested &nbsp;&nbsp;";
    echo "</td></tr>\n";

    echo "</table>";
    fclose ($handle);
  }
}
?>  